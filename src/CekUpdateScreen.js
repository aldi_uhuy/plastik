import React, {useEffect, useState} from 'react';
import {View, Text, StyleSheet, Image, BackHandler, Alert, ActivityIndicator, ImageBackground, StatusBar, Dimensions} from 'react-native';
import {getAppstoreAppMetadata} from 'react-native-appstore-version-checker';
import AsyncStorage from '@react-native-community/async-storage';
import Code from './AuthCode';

const width = Dimensions.get('window').width
const height = Dimensions.get('window').height

let dataCek = '';
//versi functional component
const useFetchData = ({navigation}) => {
  const [status, setStatus] = useState(dataCek);
  const [versi, setVersi] = useState('');
    
  const getData = async url => {
    await fetch(url,{
        headers:{
            Auth : Code
        }
    })
      .then(response => response.json())
      .then(responseJson => {
        if(responseJson.status === true){
            if(responseJson.data.status == 1){
                setVersi(responseJson.data.versi_aplikasi)
                setTimeout(() => {
                    navigation.replace('Routers')
                }, 3000);
            }else{
                alert('Sedang Maintenance')
            }
        }else{
            Alert.alert(
                "Not Connected",
                responseJson.message,
                [
                    { text: "Coba Lagi Nanti", onPress: () => BackHandler.exitApp() }
                ],
                { cancelable: false }
            );
        }
      })
      .catch(error => {
        Alert.alert(
          "Not Connected",
          "Error 501",
          [
            { text: "Try Again Later", onPress: () => BackHandler.exitApp() }
          ],
          { cancelable: false }
        );
        return true;
      });
  };

  useEffect(() => {
    getData('https://plastik.bandungbaratkab.go.id/api/cekupdateapp');
  }, []);

  return {status, versi};
};

const CekUpdateScreen = ({navigation}) => {
  const {status, versi} = useFetchData({navigation});
    return (
        <React.Fragment>
            <StatusBar translucent backgroundColor="transparent" />
            <ImageBackground style={styles.loader} source={require('../assets/intro.jpg')}>
                <View style={{marginTop: width/3, alignItems: 'center', backgroundColor:'grey', borderRadius:30, opacity:0.8, justifyContent:'center',height:200, padding:10, flexDirection:'column'}}>
                    <Image
                        source={require('../assets/icon/logo-plastikold.png')}
                        style={{resizeMode: 'contain', borderRadius:20, height:300, width:300}}
                    />
                </View>
            </ImageBackground>
        </React.Fragment>
    );
};

export default CekUpdateScreen;

const styles = StyleSheet.create({
  loader: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    padding: 1,
    opacity:0.9
  },
});
