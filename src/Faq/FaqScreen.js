import React, { useState, useEffect } from 'react';
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  ActivityIndicator,
  RefreshControl,
  Dimensions
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import { Content, Accordion, Icon } from "native-base";
import Code from '../AuthCode'
import AutoHeightWebView from 'react-native-autoheight-webview';

const FaqScreen = () =>  {
    const [faq, setFaq] = useState([])
    const [loading, setLoading] = useState(true)
    const [refreshing, setRefreshing] = useState(false)
    const [error, setError] = useState('')
    const [status, setStatus] = useState(false)

    const getFaq = async () => {
        let url = 'https://plastik.bandungbaratkab.go.id/api/faq'
        try{
            await fetch(url,{
                headers:{
                    Auth : Code
                }
            })
            .then((response) => response.json())
            .then((resp) => {
                //console.log(resp)
                if(resp.status === true){
                    setFaq(resp.data)
                    setLoading(false)
                    setRefreshing(false)
                    setError('')
                    setStatus(resp.status)
                }else{
                    setError(resp.message)
                    setLoading(false)
                    setRefreshing(false)
                    setStatus(resp.status)
                }
            })
            .catch(e => {
                console.log(e)
                alert('Error 501')
                setLoading(false)
            })
        }catch(err){
            console.log(err)
            alert(err)
            setLoading(false)
            setRefreshing(false)
        }
    }

    const renderItem = ({item}) => {

        const renderHeader = (item, expanded) => {
            return (
                <Animatable.View style={{
                    flexDirection: "row",
                    padding: 10,
                    justifyContent: "space-between",
                    alignItems: "center" ,
                    borderRadius:10,
                    margin:5,
                    backgroundColor: "#A9DAD6" }}
                    duration={300}
                    transition="backgroundColor"
                >
                    <Text style={{ fontWeight: "600", width:'90%', textAlign:'auto' }}>
                    {" "}{item.title}
                    </Text>
                    {expanded
                    ? <Icon style={{ fontSize: 18 }} name="remove-circle" />
                    : <Icon style={{ fontSize: 18 }} name="add-circle" />}
                </Animatable.View>
                );
        }

        const renderContent = (item, expanded) => {
            return (
                <Animatable.View
                    style={{ backgroundColor: (expanded ? 'rgba(255,255,255,1)' : 'rgba(245,252,255,1)'), margin:5, borderRadius:10 }}
                    duration={1000}
                    easing="ease-out"
                    animation="lightSpeedIn"
                >    
                    <AutoHeightWebView
                        style={{ width: '100%', backgroundColor:'#e3f1f1' }}
                        customStyle={`
                        * {
                            font-family: 'Times New Roman';
                            margin:2px;
                        }
                        p {
                            font-size: 14px;
                            text-align:justify;
                        }
                        `}    
                        files={[{
                            href: 'cssfileaddress',
                            type: 'text/css',
                            rel: 'stylesheet'
                        }]}
                        source={{ html: item.content }}
                        scalesPageToFit={false}
                        viewportContent={'width=device-width, user-scalable=no'}
                    />
                </Animatable.View>
            )
        }

        return(
            <>
                <View style={{margin:10}}>
                    <Text style={{fontSize:18, fontWeight:'bold'}}>{item.title}</Text>
                </View>
                <Content padder>
                    <Accordion dataArray={item.data} expanded={[]}
                        headerStyle={{backgroundColor:'#b7daf8'}}
                        renderHeader={renderHeader}
                        renderContent={renderContent}
                    />
                </Content>
            </>
        )
    }

    const viewData = () => {
        let v
        if(loading){
            v = <View style={{justifyContent:'center', alignItems:'center', flex:1}}>
                    <ActivityIndicator size="large" color="blue" />
                </View>
        }else if(!status){
            v = <View style={{justifyContent:'center', alignItems:'center', flex:1}}>
                    <Text>{error}</Text>
                </View>
        }else{
            v = <FlatList 
                    data={faq}
                    keyExtractor={(item,index) => index.toString()}
                    renderItem={renderItem}
                    refreshControl={
                        <RefreshControl
                            refreshing={refreshing}
                            onRefresh={_onRefresh}
                        />
                        }
                />
        }

        return v
    }

    const _onRefresh = () => {
        setRefreshing(true)
        setLoading(true)
    }

    useEffect(() => {
        if(!loading) return;
        getFaq()

    }, [loading, error])
    //console.log(error)

    return (
        <View style={styles.container}>
            <Text style={styles.title}>Frequently Ask Questions</Text>
            <Text style={{textAlign:'center', fontStyle:'italic', marginTop:-10, marginBottom:10}}>Yang sering ditanyakan</Text>
            {viewData()}
        </View>
    );
}

export default FaqScreen

const styles = StyleSheet.create({
    p: {
        fontWeight: '300',
        color: '#FF3366', // make links coloured pink
    },
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',
        paddingTop:30
    },
    title: {
        textAlign: 'center',
        fontSize: 22,
        fontWeight: '300',
        marginBottom: 20,
    },
    header: {
        backgroundColor: '#F5FCFF',
        padding: 10,
        margin:10,
        borderRadius:20,
    },
    headerText: {
        textAlign: 'center',
        fontSize: 16,
        fontWeight: '500',
        width:'90%'
    },
    content: {
        padding: 20,
        backgroundColor: '#fff',
        margin:10,
        borderRadius:20
    },
    active: {
        backgroundColor: 'red',
    },
    inactive: {
        backgroundColor: 'blue',
    },
    selectors: {
        marginBottom: 10,
        flexDirection: 'row',
        justifyContent: 'center',
    },
    selector: {
        backgroundColor: '#F5FCFF',
        padding: 10,
    },
    activeSelector: {
        fontWeight: 'bold',
    },
    selectTitle: {
        fontSize: 14,
        fontWeight: '500',
        padding: 10,
    },
    multipleToggle: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginVertical: 30,
        alignItems: 'center',
    },
    multipleToggle__title: {
        fontSize: 16,
        marginRight: 8,
    },
});