import React, {useState, useEffect, useRef, useCallback} from 'react';
import {
  View,
  TextInput,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  Dimensions,
  LogBox, Alert
} from 'react-native';
import {Formik} from 'formik';
import {ScrollView} from 'react-native-gesture-handler';
import * as yup from 'yup';
import ProgressBar from 'react-native-progress/Bar';
import Loading from 'react-native-whc-loading';
import {Picker, Textarea} from 'native-base';
import Code from '../AuthCode';
import DocumentPicker from 'react-native-document-picker';
import DropDownPicker from 'react-native-dropdown-picker';
import Ionicons from 'react-native-vector-icons/Ionicons'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import {useNavigation} from '@react-navigation/native'
import AsyncStorage from '@react-native-community/async-storage';
import Modal from 'react-native-modal';
import ImagePicker from 'react-native-image-crop-picker';

const {width, height} = Dimensions.get('window');

const validateform = yup.object().shape({
    nama: yup.string().required('Nama Harus Diisi'),
    nip: yup.string().required('NIP Harus Diisi'),
    no_tlp: yup.string().required('Nomor Telepon Harus Diisi'),  
    kategori: yup.string().required('Kategori Harus Dipilih'),
    pd: yup.string().required('Kategori Harus Dipilih'),
    keluhan: yup
        .string()
        .required('Deskripsikan Keluhan dengan jelas'),
});

const LaporScreen = () => {
    const navigation = useNavigation()
    const loadref = useRef(null)
    const [pd, setPd] = useState([])
    const [selectedPD, setSelectedPD] = useState('')
    const [lampiran, setLampiran] = useState([])
    const [allData, setAllData] = useState([])
    const [history, setHistory] = useState([])
    const [isModalVisible, setModalVisible] = useState(false);
    const [totallampiran, settotalLampiran] = useState(0)

    const getPD = async () => {
        const url = 'https://plastik.bandungbaratkab.go.id/api/pd';
        await fetch(url,{
            headers:{
                Auth: Code
            }
        })
        .then(response => response.json())
        .then(responseJson => {
            //console.log(responseJson)
            setPd(responseJson.data)
        })
        .catch(error => {
            console.log(error)
        });
    }

    const lampirkanfile = async () => {
      setModalVisible(false)
      try {
        const results = await DocumentPicker.pickMultiple({
          type: [DocumentPicker.types.images, DocumentPicker.types.pdf, DocumentPicker.types.docx],
        })
        .then(response => {
            //console.log(response[0].size)
          if(lampiran.length > 0){
            let x;
            setLampiran([...lampiran, ...response])
            x = (response.reduce((a,v) =>  a = a + v.size , 0 ))
            settotalLampiran(totallampiran+x)
          }else{
            let x;
            setLampiran(response)
            x = (response.reduce((a,v) =>  a = a + v.size , 0 ))
            settotalLampiran(totallampiran+x)
          }
        })
        // console.log(results)
        // if(results.length > 0){
        //   setLampiran([...lampiran, ...results])
        // }else{
        //   setLampiran(results)
        // }
        // for ( const res of results ) {
        //   console.log(
        //     res.uri,
        //     res.type, // mime type
        //     res.name,
        //     res.size
        //   );
        // }
      } catch ( err ) {
        if ( DocumentPicker.isCancel(err) ) {
          // User cancelled the picker, exit any dialogs or menus and move on
          console.log('picker cancelled')
        } else {
          throw err;
        }
      }
    }

    const removeLampiran = (id, size) => {

      const findlam = lampiran.findIndex(fv => fv.fileCopyUri === id)
      if(findlam >= 0){
        const updlam = [...lampiran]
        const remove = updlam.map((itm) => {
          return itm.fileCopyUri
        }).indexOf(id)
        updlam.splice(remove,1)
        console.log(updlam)
        setLampiran(updlam)
        settotalLampiran(totallampiran-size)
      }else{
          alert('kosong')
      }
    }

    const saveHistory = async () => {
      try {
        const jsonValue = JSON.stringify(history)
        await AsyncStorage.setItem('history', jsonValue)
      } catch(e) {
        console.log('error ',e)
      }
    }

    const getHistory = useCallback(async () => {
      try {
        const jsonValue = await AsyncStorage.getItem('history')
        const res = JSON.parse(jsonValue)
        //console.log('his awal saya adalah ',res.length)
        if(res != null){
          setHistory(res)
        }else{
          setHistory([])
        }
      } catch(e) {
        // read error
        console.log('error  ',e)
      }
    },[history])

    const _saveData = useCallback(async (values) => {

      const formDataImg = new FormData();
      for(let p in values) formDataImg.append(p,values[p]);

      Object.keys(lampiran).forEach(key => formDataImg.append(key, lampiran[key]));
      loadref.current.show();
      try {
        await fetch('https://plastik.bandungbaratkab.go.id/api/newaduan', {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              "Content-Type": 'multipart/form-data',
              Auth: Code
            },
            body: formDataImg,
        })
        .then(response => response.json())
        .then(responsJson => {
            console.log('hasil respon json '+JSON.stringify(responsJson));
            //Alert.alert('Sukses !!!'+ responsJson.message + responsJson.kode_tiket);
            setHistory([...history, responsJson.detail_tiket])
            Alert.alert(
              "Sukses !!!",
              responsJson.message,
              [
                {
                  text: "OK",
                  onPress: () => navigation.goBack(),
                  style: "cancel"
                },
                { text: "Cek Histori", onPress: () => {
                    setLampiran([])
                    navigation.navigate('Histori')
                }  }
              ],
              { cancelable: false }
            );
            //resolve(true);
            loadref.current.close();
        })
        .catch(error => {
            alert('error fetch '+ error);
            loadref.current.close();
            //reject()
        });
      }catch (error) {
        //reject()
        loadref.current.close();
        alert('error try'+error);
        console.log(error)
      }
        //})
    },[history,lampiran])

    const toggleModal = () => {
      setModalVisible(!isModalVisible);
    };

    const openCamera = () => {
      ImagePicker.openCamera({
       //width: 300,
        //height: 400,
        cropping: true
      }).then(image => {
        //console.log(image);
        settotalLampiran(totallampiran+image.size)
        let number = Math.floor(Math.random() * 100000) + 1 ;
        setLampiran([...lampiran, {
          fileCopyUri: image.path,
          uri: image.path,
          name:`PLS_CAMERA${number}`,
          type:image.mime,
          size:image.size,
        }])
        setModalVisible(false)
      })
      .catch(e => {
        console.log(e)
      })
    }

    const getTotalLampiran = (bytes, decimals = 2) => {
      if (bytes === 0) return '0 Bytes';
        const k = 1024;
        const dm = decimals < 0 ? 0 : decimals;
        const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

        const i = Math.floor(Math.log(bytes) / Math.log(k));

        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    }

    useEffect(() => {
        getPD()
        LogBox.ignoreLogs(['Animated: `useNativeDriver`']);
        return () => {
          
        }
    }, [lampiran])

    useEffect(() => {
      if(history.length !== 0){
        saveHistory()
      }
    
    }, [history])

    useEffect(() => {
      
      getHistory()
      return () => {
          
      }
  
    }, [])
    //console.log('panjang his '+JSON.stringify(history))
    //console.log('lamp', lampiran)
    return (
      <>
        <Loading ref={loadref} />
        <Formik
          initialValues={{
            nama: '',
            nip: '',
            no_tlp: '',
            keluhan: '',
            pd: '',
            kategori:''
          }}
          validationSchema={validateform}
          onSubmit={(values, actions) => {
            _saveData(values)
            .then(() => 
              actions.resetForm({})
            )
            .catch(e => alert(e))
            //.finally(() => actions.setSubmitting(false), actions.resetForm({}));
          }}>
          {formikProps => (
            <>
              <ScrollView style={{margin:10}} showsVerticalScrollIndicator={false} keyboardShouldPersistTaps="always" >
                <View style={styles.formGroup}>
                    <Text style={styles.txtLabel}>Nama Lengkap</Text>
                    <TextInput
                        style={styles.txtInput}
                        onChangeText={formikProps.handleChange('nama')}
                        onBlur={formikProps.handleBlur('nama')}
                        value={formikProps.values.nama}
                        placeholder="Masukkan Nama Lengkap"
                        returnKeyType={"next"}
                    />
                    <Text style={{color: 'red'}}>
                        {formikProps.touched.nama && formikProps.errors.nama}
                    </Text>
                </View>
                <View style={styles.formGroup}>
                    <Text style={styles.txtLabel}>NIP</Text>
                    <TextInput
                        style={styles.txtInput}
                        onChangeText={formikProps.handleChange('nip')}
                        onBlur={formikProps.handleBlur('nip')}
                        value={formikProps.values.nip}
                        keyboardType="phone-pad"
                        maxLength={16}
                        placeholder="Masukkan Nomor Induk Pegawai"
                        returnKeyType={"next"}
                    />
                    <Text style={{color: 'red'}}>
                        {formikProps.touched.nip && formikProps.errors.nip}
                    </Text>
                </View>
                <View style={styles.formGroup}>
                  <Text style={styles.txtLabel}>No Telepon (Aktif)</Text>
                  <TextInput
                    style={styles.txtInput}
                    onChangeText={formikProps.handleChange('no_tlp')}
                    onBlur={formikProps.handleBlur('no_tlp')}
                    keyboardType="phone-pad"
                    value={formikProps.values.no_tlp || ''}
                    placeholder="Masukkan Nomor Telepon yang Aktif"
                    returnKeyType={"next"}
                  />
                  <Text style={{color: 'red'}}>
                    {formikProps.touched.no_tlp && formikProps.errors.no_tlp}
                  </Text>
                </View>
                <View style={styles.formGroup}>
                  <Text style={styles.txtLabel}>Kategori</Text>
                  <Picker
                    mode="dropdown"
                    style={{width: '100%'}}
                    placeholderStyle={{color: '#bfc6ea'}}
                    placeholderIconColor="#007aff"
                    selectedValue={formikProps.values['kategori']}
                    onValueChange={itemValue =>
                      formikProps.setFieldValue('kategori', itemValue)
                    }>
                    <Picker.Item label="-- Pilih Kategori --" value="" />
                    <Picker.Item label="Aplikasi" value="1" />
                    <Picker.Item label="Jaringan" value="2" />
                  </Picker>
                  <Text style={{color: 'red'}}>
                    {formikProps.touched.kategori && formikProps.errors.kategori}
                  </Text>
                </View>
                <View style={styles.formGroup}>
                  <Text style={styles.txtLabel}>Perangkat Daerah</Text>
                  {/* <Picker
                    mode="dropdown"
                    style={{width: '100%'}}
                    placeholderStyle={{color: '#bfc6ea'}}
                    placeholderIconColor="#007aff"
                    selectedValue={formikProps.values['pd']}
                    onValueChange={itemValue =>
                      formikProps.setFieldValue('pd', itemValue)
                    }>
                    <Picker.Item label="-- Pilih Perangkat Daerah --" value="" />
                    {pd.map((item, index) => (
                      <Picker.Item
                        key={index}
                        label={item.label}
                        value={item.value}
                      />
                    ))}
                  </Picker> */}
                  <DropDownPicker
                      items={pd}
                      placeholder="-- Pilih Perangkat Daerah --"
                      defaultValue={selectedPD}
                      containerStyle={{height: 40}}
                      style={{backgroundColor: '#fafafa', borderColor:'steelblue', borderRadius:60}}
                      itemStyle={{
                          justifyContent: 'flex-start', borderBottomWidth:1, padding:3
                      }}
                      labelStyle={{color:'black'}}
                      dropDownStyle={{backgroundColor: '#fafafa', height:height/1, borderColor:'steelblue'}}
                      onChangeItem={item => formikProps.setFieldValue('pd', item.value)}
                      searchable={true}
                      searchablePlaceholder="Cari Dinas"
                      searchablePlaceholderTextColor="gray"
                      searchableError={() => <Text>Data Tidak Ditemukan</Text>}
                      activeLabelStyle={{
                          color: 'red'
                      }}
                      arrowStyle={{marginRight:5}}
                  />
                  <Text style={{color: 'red'}}>
                    {formikProps.touched.pd && formikProps.errors.pd}
                  </Text>
                </View>
                <View style={styles.formGroup}>
                  <Text style={styles.txtLabel}>Keluhan</Text>
                  <Textarea
                    style={{width: '100%', borderColor: 'steelblue'}}
                    rowSpan={5}
                    bordered
                    borderRadius={10}
                    onChangeText={formikProps.handleChange('keluhan')}
                    onBlur={formikProps.handleBlur('keluhan')}
                    value={formikProps.values.keluhan || ''}
                    placeholder="Jelaskan masalah sejelas mungkin"
                    returnKeyType={"next"}
                  />
                  <Text style={{color: 'red'}}>
                    {formikProps.touched.keluhan && formikProps.errors.keluhan}
                  </Text>
                </View>
                <View style={styles.formGroup}>
                  <View style={{flexDirection:'row'}}>
                    <TouchableOpacity
                      style={{
                        backgroundColor: 'orange',
                        paddingHorizontal: 12,
                        paddingVertical: 11,
                        alignSelf: 'flex-start',
                        borderRadius: 7,
                      }}
                      onPress={toggleModal}>
                      <Text
                        style={{
                          fontSize: 13,
                          fontWeight: 'bold',
                          color: 'white',
                          textAlign: 'center',
                        }}>
                        Lampirkan File Bukti
                      </Text>
                    </TouchableOpacity>
                    <View style={{flexDirection:'column'}}>
                      <Text style={{marginLeft:5}}>(Max Total Lampiran : 10 MB)</Text>
                      <Text style={{marginLeft:5}}>(Total Lampiran : {getTotalLampiran(totallampiran)})</Text>
                    </View>
                  </View>
                  {totallampiran > 10000000 ? <Text style={{color:'red'}}>Melebihi Max Lampiran</Text> : null}
                  
                  <ScrollView style={{flexDirection:'row', marginTop:10}} horizontal showsHorizontalScrollIndicator={false} >
                    {lampiran.length > 0 ? 
                      lampiran.map((item, index) => {
                        let x;
                        if(item.type === 'application/pdf'){
                          x = <View style={{flexDirection:'column'}}>
                                <View style={{width:80, height:80, borderRadius:20, borderWidth:1, alignItems:'center', justifyContent:'center'}}>
                                  <FontAwesome name="file-pdf-o" size={40} color="red" />
                                </View>
                                <Text style={{fontSize:12, width:width/4}} numberOfLines={1} >{item.name}</Text>
                              </View>
                        }else if(item.type === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'){
                          x = <View style={{flexDirection:'column'}}>
                                <View style={{width:80, height:80, borderRadius:20, borderWidth:1, alignItems:'center', justifyContent:'center'}}>
                                  <FontAwesome name="file-word-o" size={40} color="blue" />
                                </View>
                                <Text style={{fontSize:12, width:width/4}} numberOfLines={1} >{item.name}</Text>
                              </View>
                        }else{
                          x = <View style={{flexDirection:'column'}}>
                                <View style={{width:80, height:80, borderRadius:20, borderWidth:1, alignItems:'center', justifyContent:'center'}}>
                                  <Image source={{uri:item.uri}} style={{width:80,height:80, borderRadius:20, margin:10, borderWidth:1, borderColor:'black'}} />
                                </View>
                                <Text style={{fontSize:12, width:width/4}} numberOfLines={1} >{item.name}</Text>
                              </View>
                        }
                        return <View key={index} style={{flexDirection:'row-reverse', alignItems:'flex-start'}}>
                                <TouchableOpacity
                                  style={{marginLeft:-10}}
                                  onPress={() => removeLampiran(item.fileCopyUri, item.size)}
                                >
                                  <Ionicons name="close" size={30} />
                                </TouchableOpacity>
                                {x}
                              </View>
                            
                      })
                    : null }
                  </ScrollView>
                </View>
                <View style={styles.formGroup}>
                  <TouchableOpacity
                    style={{
                      backgroundColor: totallampiran > 10000000 ? 'red' : '#61A756',
                      paddingHorizontal: 12,
                      paddingVertical: 11,
                      alignSelf: 'flex-start',
                      borderRadius: 7,
                    }}
                    onPress={formikProps.handleSubmit}
                    disabled={totallampiran > 10000000 ? true : false}
                  >
                    <Text
                      style={{
                        fontSize: 13,
                        fontWeight: 'bold',
                        color: 'white',
                        textAlign: 'center',
                      }}>
                      Laporkan
                    </Text>
                  </TouchableOpacity>
                </View>
              </ScrollView>
            </>
          )}
        </Formik>
        <Modal 
          isVisible={isModalVisible}
          animationIn="bounceIn"
          deviceHeight={height}
          deviceWidth={width}
          style={{alignContent:'flex-end', flexDirection:'column', justifyContent:'flex-end', margin:2}}

        >
          <View style={{backgroundColor:'white', height:height/4, width:'100%', borderRadius:20}}>
            <View style={{margin:10}}>
              <TouchableOpacity 
                onPress={openCamera}
                style={{borderBottomWidth:1, padding:10, flexDirection:'row', justifyContent:'space-between'}}
              >
                <Text>Ambil Dari Kamera</Text>
                <Ionicons name="md-chevron-forward-outline" size={20} />
              </TouchableOpacity>
              <TouchableOpacity 
                onPress={() => lampirkanfile()}
                style={{borderBottomWidth:1, padding:10, flexDirection:'row', justifyContent:'space-between'}}
              >
                <Text>Ambil Dari Galeri</Text>
                <Ionicons name="md-chevron-forward-outline" size={20} />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={toggleModal}
                style={{borderBottomWidth:1, padding:10, flexDirection:'row', justifyContent:'space-between'}}
              >
                <Text>Batalkan</Text>
                <Ionicons name="md-chevron-forward-outline" size={20} />
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </>
    )
}

export default LaporScreen

const styles = StyleSheet.create({
    formGroup: {
      margin: 5,
    },
    txtLabel: {
      marginBottom: 5,
    },
    txtInput: {
      borderWidth: 1,
      padding: 5,
      borderColor: 'steelblue',
      borderRadius: 10,
    },
  });