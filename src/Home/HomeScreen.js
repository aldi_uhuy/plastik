import React from 'react'
import { View, Text, Dimensions, Image, StyleSheet } from 'react-native'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import ViewSlider from 'react-native-view-slider'
import { FloatingAction } from "react-native-floating-action"
import Ionicons from 'react-native-vector-icons/Ionicons'
import * as Animatable from 'react-native-animatable';

const width = Dimensions.get('screen').width;
const height = Dimensions.get('screen').height;

const actions = [
    {
        text: "Laporkan Aduan",
        icon: <Ionicons name="send" color="white" />,
        name:'aduan',
        position: 1
    }
];

const HomeScreen = ({navigation}) => {
    return (
        <>
            <ScrollView>
                <ViewSlider 
                    renderSlides = {
                        <>
                            <View style={{width:width/1.2, height:height/3.5, margin:10, flexDirection:'row', alignItems:'center', justifyContent:'center', marginTop:20}}>
                                <Image source={require('../../assets/home/banner-24.jpg')} style={{height: 700 ,width:'100%', borderRadius:30, resizeMode:'contain'}}/>
                                <View style={{position:'absolute', alignItems:'center'}}>
                                    <Text style={{color:'white', fontSize:30, fontWeight:'bold', marginTop:-80}}>PLASTIK</Text>
                                    <Text style={{color:'white', fontSize:15, fontWeight:'bold'}}>(Pengaduan Layanan Diskominfotik)</Text>
                                </View>
                            </View>
                            <View style={{flexDirection:'column', justifyContent:'center', alignItems:'center'}}>
                                <View style={{width:width/1.2, height:height/3.4, borderWidth:2, borderColor:'red', margin:10, backgroundColor:'#e0e02b', flexDirection:'row-reverse', alignItems:'center', borderRadius:30, marginTop:1}}>
                                    <View style={{flexDirection:'column', width:'60%', marginVertical:10}}>
                                        <View>
                                            <Text style={{fontWeight:'bold', marginBottom:10, textAlign:'center'}}>Gangguan Jaringan Internet ???</Text>
                                            <Text style={{fontWeight:'bold', marginBottom:10}}>Ada Kendala aplikasi ???</Text>
                                        </View>
                                        <View style={{flex:1, justifyContent:'flex-end', flexDirection:'column', margin:10}}>
                                            <TouchableOpacity style={{backgroundColor:'green', alignItems:'center', width:'70%', padding:5, borderRadius:10, alignSelf:'flex-end'}}
                                                onPress={() => navigation.navigate('lapor')}
                                            >
                                                <Text style={{color:'white'}}>Lapor Disini</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                    <Animatable.View style={{width:'43%'}} animation="pulse" iterationCount="infinite" easing="ease-out" >
                                        <Image source={require('../../assets/home/inter.png',)} style={{width:100, height:100, marginLeft:20}} />
                                    </Animatable.View>
                                </View>
                            </View>
                        </>
                    }
                    style={styles.slider}     //Main slider container style
                    height = {200}    //Height of your slider
                    slideCount = {2}    //How many views you are adding to slide
                    dots = {true}     // Pagination dots visibility true for visibile 
                    dotActiveColor = 'red'     //Pagination dot active color
                    dotInactiveColor = 'gray'    // Pagination do inactive color
                    dotsContainerStyle={styles.dotContainer}     // Container style of the pagination dots
                    //autoSlide = {true}    //The views will slide automatically
                    slideInterval = {2000}    //In Miliseconds
                />
                <View style={{flexDirection:'column', backgroundColor:'grey', justifyContent:'center', borderRadius:25, margin:20, marginTop:-5}}>
                    <View style={{alignItems:'center', margin:10}}>
                        <Text style={{color:'white', fontWeight:'bold', fontSize:20, textAlign:'center'}}>Cara Menggunakan Aplikasi Plastik</Text>
                    </View>
                    <View style={{flexDirection:'column', alignItems:'center'}}>
                        <Image
                            source={require('../../assets/home/manual.jpg')}
                            style={{width:300,height:270, resizeMode:'contain', borderRadius:20, margin:10}}
                        />
                    </View>
                    <View style={{padding:15}}>
                        <Text style={{color:'black', fontWeight:'bold', fontSize:13}}>https://plastik.bandungbaratkab.go.id</Text>
                        <Text style={{textAlign:'justify'}}>Buka pada browser alamat website Plastik / Buka App Android Plastik</Text>
                    </View>
                    <View style={{padding:15}}>
                        <Text style={{color:'black', fontWeight:'bold', fontSize:13}}>Persiapkan Berkas Pengajuan</Text>
                        <Text style={{textAlign:'justify'}}>Persiapkan dokumen seperti foto bukti masalah</Text>
                    </View> 
                    <View style={{padding:15}}>
                        <Text style={{color:'black', fontWeight:'bold', fontSize:13}}>Pengajuan Tiket</Text>
                        <Text style={{textAlign:'justify'}}>Buka menu "Pengajuan Tiket" kemudian isi aduan anda sesuai form yang tersedia.</Text>
                    </View>   
                    <View style={{padding:15}}>
                        <Text style={{color:'black', fontWeight:'bold', fontSize:13}}>Cek Status Pengajuan</Text>
                        <Text style={{textAlign:'justify'}}>Buka Menu "Monitoring" untuk melihat hasil dari pengaduan.</Text>
                    </View>   
                    <View style={{padding:15}}>
                        <Text style={{color:'black', fontWeight:'bold', fontSize:13}}>Berikan Penilaian</Text>
                        <Text style={{textAlign:'justify'}}>Berikan penilaian kepada pegawai yang meng handle terhadap pengajuan Anda.</Text>
                    </View>   
                </View>
            </ScrollView>
            <FloatingAction
                actions={actions}
                onPressItem={() => navigation.navigate('lapor')}
            />
        </>
    )
}


export default HomeScreen

const styles = StyleSheet.create({
    viewBox: {
        paddingHorizontal: 20,
        justifyContent: 'center',
        width: width,
        padding: 10,
        alignItems: 'center',
        height: 150
    },
    slider: {
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        padding:10,
        height:300,
        width:'100%',
        backgroundColor:'white'
    },
    dotContainer: {
        backgroundColor: 'transparent'
    }
});