import React from 'react';
import {
  StyleSheet,
  Platform,
  InteractionManager,
  Alert,
  BackHandler,
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';

import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import {
  createStackNavigator,
  CardStyleInterpolators,
} from '@react-navigation/stack';
import _ from 'lodash';
import HomeScreen from './Home/HomeScreen.js';
import LaporScreen from './Home/LaporScreen.js';
import MonitoringScreen from './Monitoring/MonitoringScreen.js';
import PDFScreen from './Monitoring/PDFScreen.js';
import ImageScreen from './Monitoring/ImageScreen.js';
import FaqScreen from './Faq/FaqScreen.js';
import HistoriScreen from './Histori/HistoriScreen.js';
import MonitoringHistoriScreen from './Histori/MonitoringHistoriScreen.js';
import HeaderRightScreen from './Histori/HeaderRightScreen.js';
import {getFocusedRouteNameFromRoute} from '@react-navigation/native';

const _setTimeout = global.setTimeout;
const _clearTimeout = global.clearTimeout;
const MAX_TIMER_DURATION_MS = 60 * 1000;
if (Platform.OS === 'android') {
  // Work around issue `Setting a timer for long time`
  // see: https://github.com/firebase/firebase-js-sdk/issues/97
  const timerFix = {};
  const runTask = (id, fn, ttl, args) => {
    const waitingTime = ttl - Date.now();
    if (waitingTime <= 1) {
      InteractionManager.runAfterInteractions(() => {
        if (!timerFix[id]) {
          return;
        }
        delete timerFix[id];
        fn(...args);
      });
      return;
    }

    const afterTime = Math.min(waitingTime, MAX_TIMER_DURATION_MS);
    timerFix[id] = _setTimeout(() => runTask(id, fn, ttl, args), afterTime);
  };

  global.setTimeout = (fn, time, ...args) => {
    if (MAX_TIMER_DURATION_MS < time) {
      const ttl = Date.now() + time;
      const id = '_lt_' + Object.keys(timerFix).length;
      runTask(id, fn, ttl, args);
      return id;
    }
    return _setTimeout(fn, time, ...args);
  };

  global.clearTimeout = (id) => {
    if (typeof id === 'string' && id.startsWith('_lt_')) {
      _clearTimeout(timerFix[id]);
      delete timerFix[id];
      return;
    }
    _clearTimeout(id);
  };
}

const Stack = createStackNavigator();
const TabBottom = createMaterialBottomTabNavigator();

const HomeStackNavigator = ({navigation, route}) => {
  
    return (
      <Stack.Navigator
        screenOptions={{
          headerStyle: {backgroundColor: '#3F51B5', height: 50},
          headerTintColor: 'white',
        }}>
        <Stack.Screen
          options={({route}) => ({
            title: 'Home Screen',
            headerTitleStyle: {fontSize: 15, fontFamily: 'Arial'},
            //headerTitleStyle:{marginLeft:120},
            headerShown:false
          })}
          name="Home"
          component={HomeScreen}
        />

        <Stack.Screen
          options={({route}) => ({
            title: 'Laporkan Aduan Anda',
            headerTitleStyle: {fontSize: 15, fontFamily: 'Arial'},
            //headerTitleStyle:{marginLeft:120},
            headerShown:true
          })}
          name="lapor"
          component={LaporScreen}
        />
      </Stack.Navigator>
    );
};

const MonitoringStackNavigator = ({navigation, route}) => {
  
    return (
      <Stack.Navigator
        screenOptions={{
          headerStyle: {backgroundColor: 'skyblue', height: 50},
          headerTintColor: 'white',
        }}>
        <Stack.Screen
          options={({route}) => ({
            title: 'Monitoring Pengaduan',
            headerTitleStyle: {fontSize: 15, fontFamily: 'Arial'},
            //headerTitleStyle:{marginLeft:120},
            headerShown:true,
          })}
          name="Monitoring"
          component={MonitoringScreen}
        />
        <Stack.Screen
          options={({route}) => ({
            title: 'Lampiran PDF',
            headerTitleStyle: {fontSize: 15, fontFamily: 'Arial'},
            //headerTitleStyle:{marginLeft:120},
            headerShown:true,
          })}
          name="viewpdf"
          component={PDFScreen}
        />
        <Stack.Screen
          options={({route}) => ({
            title: 'Lampiran Gambar',
            headerTitleStyle: {fontSize: 15, fontFamily: 'Arial'},
            //headerTitleStyle:{marginLeft:120},
            headerShown:true,
          })}
          name="viewimage"
          component={ImageScreen}
        />
      </Stack.Navigator>
    );
};

const FaqStackNavigator = ({navigation, route}) => {
  
    return (
      <Stack.Navigator
        screenOptions={{
          headerStyle: {backgroundColor: '#3F51B5', height: 50},
          headerTintColor: 'white',
        }}>
        <Stack.Screen
          options={({route}) => ({
            title: 'FAQ Plastik',
            headerTitleStyle: {fontSize: 15, fontFamily: 'Arial'},
            //headerTitleStyle:{marginLeft:120},
            headerShown:false
          })}
          name="Faq"
          component={FaqScreen}
        />
      </Stack.Navigator>
    );
};

const HistoriStackNavigator = ({navigation, route}) => {
  
    return (
      <Stack.Navigator
        screenOptions={{
          headerStyle: {backgroundColor: '#e483c0', height: 50},
          headerTintColor: 'white',
        }}>
        <Stack.Screen
          options={({route}) => ({
            title: 'Histori Aduan',
            headerTitleStyle: {fontSize: 15, fontFamily: 'Arial'},
            //headerTitleStyle:{marginLeft:120},
            headerShown:true,
            //headerRight: () => <HeaderRightScreen />,
          })}
          name="cekhistori"
          component={HistoriScreen}
        />
        <Stack.Screen
          options={({route}) => ({
            title: 'Monitoring Aduan',
            headerTitleStyle: {fontSize: 15, fontFamily: 'Arial'},
            //headerTitleStyle:{marginLeft:120},
            headerShown:true,
            //headerRight: () => <HeaderRightScreen />,
          })}
          name="monitoringhistori"
          component={MonitoringHistoriScreen}
        />
        <Stack.Screen
          options={({route}) => ({
            title: 'Lampiran PDF',
            headerTitleStyle: {fontSize: 15, fontFamily: 'Arial'},
            //headerTitleStyle:{marginLeft:120},
            headerShown:true,
          })}
          name="viewpdfhistory"
          component={PDFScreen}
        />
        <Stack.Screen
          options={({route}) => ({
            title: 'Lampiran Gambar',
            headerTitleStyle: {fontSize: 15, fontFamily: 'Arial'},
            //headerTitleStyle:{marginLeft:120},
            headerShown:true,
          })}
          name="viewimagehistory"
          component={ImageScreen}
        />
      </Stack.Navigator>
    );
};

const HomeTabNavigator = ({route}) => {
  return (
    <TabBottom.Navigator
      screenOptions={({route}) => ({
        tabBarIcon: ({focused}) => {
          let iconName;
          let warna;
          let ukuran;
          if (route.name === 'Home') {
            iconName = focused ? 'home-outline' : 'home-outline';
            warna = focused ? 'green' : 'blue';
            ukuran = focused ? 23 : 25;
          } else if (route.name === 'Monitoring') {
            iconName =  focused ? 'search' : 'search';
            warna = focused ? 'green' : 'blue';
            ukuran = focused ? 23 : 25;
          } else if (route.name === 'Faq') {
            iconName = focused ? 'help-outline' : 'help-outline';
            warna = focused ? 'green' : 'blue';
            ukuran = focused ? 23 : 25;
          } else if (route.name === 'Histori') {
            iconName = focused ? 'refresh-circle-outline' : 'refresh-circle-outline';
            warna = focused ? 'green' : 'blue';
            ukuran = focused ? 23 : 25;
          }

          return <Ionicons name={iconName} size={ukuran} color={warna} />;
        },
      })}
      activeColor="black"
      >
      <TabBottom.Screen
        name="Home"
        component={HomeStackNavigator}
        options={{
          tabBarColor:'#ee9d2f'
        }}
      />
      <TabBottom.Screen
        name="Monitoring"
        component={MonitoringStackNavigator}
        options={{
          tabBarColor:'skyblue'
        }}
      />
      <TabBottom.Screen
        name="Histori"
        component={HistoriStackNavigator}
        options={{
          tabBarColor:'#e483c0'
        }}
      />
      <TabBottom.Screen
        name="Faq"
        component={FaqStackNavigator}
        options={{
          tabBarColor:'#ffad86'
        }}
      />
    </TabBottom.Navigator>
  );
};

function getHeaderTitle(route) {
  const routeName = getFocusedRouteNameFromRoute(route) ?? 'Home';

  switch (routeName) {
    case 'Home':
      return 'Home';
    case 'Monitoring':
      return 'Monitoring';
    case 'Faq':
      return 'Faq';
    case 'Histori':
      return 'Histori';
  }
}

function shouldHeaderBeShown(route) {
  const routeName = getFocusedRouteNameFromRoute(route) ?? 'Home';

  switch (routeName) {
    case 'Home':
      return false;
    case 'Monitoring':
      return false;
    case 'Faq':
      return false;
    case 'Histori':
      return false;
  }
}

const _exit = () => {
  Alert.alert(
    'Keluar Aplikasi :(',
    'Yakin mau keluar aplikasi ini kak??',
    [
      {
        text: 'Engga jadi deh',
        onPress: () => false,
        style: 'cancel',
      },
      {text: 'Ya', onPress: () => BackHandler.exitApp()},
    ],
    {cancelable: false},
  );
  return true;
};

export default function App() {
  return (
    <Stack.Navigator
      screenOptions={{
        gestureEnabled: true,
        gestureDirection: 'horizontal',
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
      }}
      headerMode="float"
      animation="fade">
      <Stack.Screen
        options={({route}) => ({
          title: getHeaderTitle(route),
          headerShown: shouldHeaderBeShown(route),
        })}
        name="Home"
        component={HomeTabNavigator}
      />
    </Stack.Navigator>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f5fcff',
    justifyContent: 'center',
  },
  welcomeadmin: {
    fontSize: 20,
    color: '#000',
    textAlign: 'center',
    margin: 10,
  },
  loader: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 1,
  },
  drawermenu: {
    flexDirection: 'row',
    paddingVertical: 10,
    alignItems: 'center',
    padding: 10,
  },
});
