import React, {useState} from 'react';
import { Text, StyleSheet, Dimensions, View, ActivityIndicator, Alert, TextInput } from 'react-native';
import Pdf from 'react-native-pdf';

const PDFScreen = ({route}) => {

    const source = {uri:route.params.path, cache:true};
    const [page, setPage] = useState(1)
    const [numpage, setNumpage] = useState(0)
    const [pagesearch, setPagesearch] = useState(1)

    return (
        <View style={styles.container}>
            <View style={{flexDirection:'row', justifyContent:'space-around', marginBottom:10}}>
                <View style={{flexDirection:'row', alignItems:'center'}}>
                    <Text>Page : {page}</Text>
                    {/* <TextInput 
                        style={{height:35, marginTop:4}} 
                        value={this.state.pagesearch} 
                        //onChangeText={text => this.setState({pagesearch:text})} 
                        keyboardType="number-pad"
                        //returnKeyType = {"next"}
                        onSubmitEditing={(event) => this.handleSearch(event.nativeEvent.text)} 
                    /> */}
                </View>
                <View style={{alignItems:'center', flexDirection:'row'}}>
                    <Text>Number of Page : {numpage}</Text>
                </View>
            </View>
            <Pdf
                source={source}
                onLoadComplete={(numberOfPages,filePath)=>{
                    console.log(`number of pages: ${numberOfPages}`);
                }}
                onPageChanged={(page,numberOfPages)=>{
                    console.log(`current page: ${page}`);
                    setNumpage(numberOfPages)
                    setPage(page)
                }}
                onError={(error)=>{
                    console.log(error);
                    Alert.alert('Error',''+error);
                }}
                page={pagesearch}
                //onPageSingleTap={() => alert('buka')}
                style={styles.pdf}
                enableRTL={true}
                activityIndicator={
                    <ActivityIndicator size="large" color="green" />
                }
            />
        </View>
    )
}

export default PDFScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        //alignItems: 'center',
        marginTop: 5,
    },
    pdf: {
        flex:1,
        //backgroundColor:'red',
        width:Dimensions.get('window').width,
        height:Dimensions.get('window').height,
        marginBottom:5
    }
});