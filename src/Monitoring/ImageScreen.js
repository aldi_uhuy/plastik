import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'

const ImageScreen = ({route}) => {
    return (
        <Image source={{uri:route.params.path}} style={StyleSheet.absoluteFillObject} />
    )
}

export default ImageScreen
