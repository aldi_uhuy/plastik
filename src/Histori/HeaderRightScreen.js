import React, {useRef} from 'react';
import {View, Text, Alert} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Menu, {MenuItem, MenuDivider} from 'react-native-material-menu';
import {TouchableOpacity} from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-community/async-storage';

const HeaderRightScreen = () => {
  const menuRef = useRef(null);
  const showMenu = () => menuRef.current.show();

  const clearHistory = async () => {
    let clear = [];
    try {
        await AsyncStorage.setItem('history',JSON.stringify(clear))
    } catch(e) {
    // clear error
    }
    menuRef.current.hide();
}

  const info = () => {
    Alert.alert(
      'Penting',
      'Histori disimpan didalam memori telepon Perangkat ini. Jika menghapus data aplkasi pada pengaturan, data histori akan hilang',
    );
    menuRef.current.hide();
  };
  return (
    <View
      style={{flexDirection: 'row', justifyContent: 'flex-end', margin: 10}}>
      <Menu
        ref={menuRef}
        button={
          <TouchableOpacity onPress={showMenu}>
            <Ionicons name="ellipsis-vertical-sharp" size={25} color="white" />
          </TouchableOpacity>
        }>
        <MenuItem onPress={clearHistory}>Hapus Semua Histori</MenuItem>
        <MenuDivider />
        <MenuItem onPress={info}>Info</MenuItem>
      </Menu>
    </View>
  );
};

export default HeaderRightScreen;
