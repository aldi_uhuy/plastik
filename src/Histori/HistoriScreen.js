import React, {useState, useEffect, useCallback, useRef} from 'react'
import { View, Text, FlatList, Dimensions, ActivityIndicator, ToastAndroid, Alert } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Ionicons from 'react-native-vector-icons/Ionicons'
import { useIsFocused, useNavigation } from '@react-navigation/native';
import Menu, {MenuItem, MenuDivider} from 'react-native-material-menu';
import Clipboard from '@react-native-clipboard/clipboard';


const {width, height} = Dimensions.get('window');

const HistoriScreen = () => {
    const menuRef = useRef(null);
    const showMenu = () => menuRef.current.show();
    const navigation = useNavigation();
    const isFocused = useIsFocused();
    const [history, setHistory] = useState([])
    const [loading, setLoading] = useState(true)

    const getHistory = async () => {
        try {
            const jsonValue = await AsyncStorage.getItem('history')
            const res = JSON.parse(jsonValue)
            //console.log('fav adalah ',res)
            if(res != null){
                res.sort(function(a,b) {
                    return a.id < b.id
                })
                setHistory(res)
                setLoading(false)
            }else{
                setHistory([])
                setLoading(false)
            }
        }catch(e) {
            // read error
            setLoading(false)
            console.log('error  ',e)
        }
    }

    const removeHistory = useCallback(async (id) => {
        const findhis = history.findIndex(fv => fv.id === id)
        if(findhis >= 0){
            const updhis = [...history]
            const remove = updhis.map((itm) => {
                return itm.id
            }).indexOf(id)
            updhis.splice(remove,1)

            const jsonValue = JSON.stringify(updhis)
            await AsyncStorage.setItem('history', jsonValue)
            setHistory(updhis)
            ToastAndroid.show('Histori dihapus', ToastAndroid.SHORT)
        }
    },[history])

    const copyClipboard = (text) => {
        Clipboard.setString(text)
        ToastAndroid.show('Kode '+ text + ' Berhasil Disalin', ToastAndroid.LONG)
    }

    const renderItem = ({item}) => {
        return(
            <View style={{flexDirection:'row', alignItems:'center', width}}>
                <TouchableOpacity style={{width:width/1.3, height:height/8.5, backgroundColor:'#b0b0b0', borderRadius:20, margin:10, padding:10}}
                onPress={() => navigation.navigate('monitoringhistori',{kode: item.kode_pengaduan})}
                >
                    <View style={{flexDirection:'row', alignItems:'center'}}>
                        <Text style={{fontSize:16, fontWeight:'bold'}}>Kode Pengaduan : </Text>
                        <Text style={{fontSize:14, width:'50%'}} numberOfLines={2} selectable>{item.kode_pengaduan}</Text>
                    </View>
                    <View style={{flexDirection:'row', alignItems:'center'}}>
                        <Text style={{fontSize:16, fontWeight:'bold'}}>Waktu : </Text>
                        <Text style={{fontSize:14}}>{item.waktu}</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => copyClipboard(item.kode_pengaduan)}
                    style={{marginRight:5}}
                >
                    <Ionicons name="copy" size={25} color="black" />
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => removeHistory(item.id)}
                >
                    <Ionicons name="close-circle" size={30} color="red" />
                </TouchableOpacity>
            </View>
        )
    }

    const keyExtractor = useCallback((item) => item.id.toString(),[])

    const viewData = useCallback(() => {
        let v;
        if(loading && isFocused){
            v = <View style={{margin:10, flex:1, justifyContent:'center', alignItems:'center'}}>
                    <ActivityIndicator size="large" color="blue" />
                </View>
        }else if(history.length === 0 && isFocused){
            v = <View style={{margin:10, flex:1, justifyContent:'center', alignItems:'center'}}>
                    <Text>Histori Kosong</Text>
                </View>
        }else if(history.length !== 0 && isFocused && !loading){
            v = <FlatList
                    data={history}
                    renderItem={renderItem}
                    keyExtractor={keyExtractor}
                    //ListFooterComponent={renderFooter}
                />
        }

        return v
    },[isFocused, loading, history])

    const clearHistory = async () => {
        setLoading(true)
        let clear = [];
        try {
            await AsyncStorage.setItem('history',JSON.stringify(clear))
            setHistory(clear)
            setLoading(false)
        } catch(e) {
        // clear error
            console.log(e)
            setLoading(false)
        }
        menuRef.current.hide();
    }
    
    const info = () => {
        Alert.alert(
            'Penting',
            'Histori disimpan didalam memori telepon Perangkat ini. Jika menghapus data aplkasi pada pengaturan, data histori akan hilang',
        );
        menuRef.current.hide();
    };

    useEffect(() => {
        if (!loading) return;
        setTimeout(() => {
            getHistory()
        }, 1000);

    }, [history, loading])

    useEffect(() => {
        isFocused ? setLoading(true) : setLoading(false)
    }, [isFocused])

    React.useLayoutEffect(() => {
        navigation.setOptions({
            headerRight: () => (
                <View
                    style={{flexDirection: 'row', justifyContent: 'flex-end', margin: 10}}>
                    <Menu
                        ref={menuRef}
                        button={
                        <TouchableOpacity onPress={showMenu}>
                            <Ionicons name="ellipsis-vertical-sharp" size={25} color="white" />
                        </TouchableOpacity>
                        }>
                        <MenuItem onPress={clearHistory}>Hapus Semua Histori</MenuItem>
                        <MenuDivider />
                        <MenuItem onPress={(info)}>Info</MenuItem>
                    </Menu>
                </View>
            ),
        });
    },[navigation, history]);

    return (
        <>
            {viewData()}
        </>
    )
}

export default HistoriScreen
