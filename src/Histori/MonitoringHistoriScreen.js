import React, {useState, useEffect} from 'react'
import { View, Text, ActivityIndicator, ToastAndroid, ScrollView, TouchableOpacity } from 'react-native'
import Awesome from 'react-native-vector-icons/FontAwesome'
import Code from '../AuthCode'
import PercentageCircle from 'react-native-percentage-circle';
import { useNavigation } from '@react-navigation/native'

const MonitoringHistoriScreen = ({route}) => {
    const navigation = useNavigation()
    const [datapengaduan, setDatapengaduan] = useState([])
    const [loading, setLoading] = useState(false)
    const [status, setStatus] = useState(false)
    const [message, setMessage] = useState('')
    const [tindaklanjut, setTindaklanjut] = useState([])
    const [lampiran, setLampiran] = useState([])

    const getData = async () => {
        setLoading(true)
        let url = "https://plastik.bandungbaratkab.go.id/api/monitoring"
        let data = new FormData()
        data.append('keysearch', route.params.kode)
        try{
            await fetch(url,{
                method:'POST',
                headers:{
                    Auth: Code
                },
                body: data
            })
            .then(response => response.json())
            .then(responseData => {
                console.log(responseData)
                setLoading(false)
                if(responseData.status === true){
                    setDatapengaduan(responseData.data.datapengaduan)
                    setStatus(responseData.status)
                    setMessage(responseData.message)
                    setTindaklanjut(responseData.data.tindak_lanjut)
                    setLampiran(responseData.data.lampiran)
                }else{
                    setDatapengaduan([])
                    setStatus(responseData.status)
                    setMessage(responseData.message)
                    ToastAndroid.show(responseData.message, ToastAndroid.LONG)
                }
            })
        }catch(e){
            console.log(e)
        }
    }

    const viewData = () => {
        let v
        if(loading){
            v = <View style={{flex:1, justifyContent:'center', alignItems:'center'}}>
                    <ActivityIndicator size="large" color="blue" />
                </View>
        }else{
            if(status){
                v = <>
                        <ScrollView showsVerticalScrollIndicator={false}>
                            <View style={{margin:10, flexDirection:'row', justifyContent:'space-between'}}>
                                <View>
                                    <Text style={{fontSize:18, fontWeight:'bold'}}>Kode Pengaduan Anda</Text>
                                    <Text style={{marginTop:5}}>{datapengaduan.kode_pengaduan ? datapengaduan.kode_pengaduan : null}</Text>
                                </View>
                                <View style={{marginRight:30, alignItems:'center'}}>
                                    <PercentageCircle radius={25} percent={datapengaduan.persentase_selesai} color={datapengaduan.persentase_selesai === '100' ? 'green' : 'blue'}></PercentageCircle>
                                    <Text>{datapengaduan.persentase_selesai === '100' ? 'Selesai' : 'Dalam Proses'}</Text>
                                </View>
                            </View>
                            <View style={{margin:10}}>
                                <Text style={{fontSize:18, fontWeight:'bold'}}>Uraian Pengaduan Anda</Text>
                                <Text style={{textAlign:'justify', margin:10}}>{datapengaduan.uraian ? datapengaduan.uraian : null}</Text>
                            </View>
                            <View style={{margin:10}}>
                                <Text style={{fontSize:18, fontWeight:'bold'}}>Tindak Lanjut Pengaduan</Text>
                                <View style={{flexDirection:'row'}}>
                                    <View style={{marginTop:10, borderWidth:1, backgroundColor:'grey', width:'30%'}}>
                                        <Text style={{textAlign:'center', margin:5}}>Petugas</Text>
                                    </View>
                                    <View style={{marginTop:10, borderWidth:1, backgroundColor:'grey', width:'70%'}}>
                                        <Text style={{textAlign:'center', margin:5}}>Tindak Lanjut</Text>
                                    </View>
                                </View>
                                {
                                    tindaklanjut.map((item, i) => {
                                        if(item.status === true){
                                            return(
                                                <View style={{flexDirection:'row', marginTop:-10}} key={i}>
                                                    <View style={{marginTop:10, borderWidth:1,width:'30%'}}>
                                                        <Text style={{textAlign:'center', margin:10}}>{item.petugas}</Text>
                                                    </View>
                                                    <View style={{marginTop:10, borderWidth:1, width:'70%'}}>
                                                        <Text style={{textAlign:'justify', margin:10}}>{item.tindak_lanjut}</Text>
                                                    </View>
                                                </View>
                                            )
                                        }else{
                                            return(
                                                <View style={{flexDirection:'row', marginTop:-10}} key={i}>
                                                    <View style={{marginTop:10, borderWidth:1, width:'100%'}}>
                                                        <Text style={{textAlign:'center', margin:10}}>{item.message}</Text>
                                                    </View>
                                                </View>
                                            )
                                        }
                                    })
                                }   
                            </View>
                            <View style={{margin:10}}>
                                <Text style={{fontSize:18, fontWeight:'bold'}}>Lampiran</Text>
                                <ScrollView style={{flexDirection:'row'}} horizontal showsHorizontalScrollIndicator={false}>
                                    {
                                        lampiran.map((item, i) => {
                                            let icon;
                                            let v;
                                            if(item.type === 'pdf'){
                                                icon = <Awesome name="file-pdf-o" size={30} color="red" />
                                            }else if(item.type === 'png' || item.type === 'jpg' || item.type === 'jpeg'){
                                                icon = <Awesome name="file-image-o" size={30} color="black" />
                                            }

                                            if(item.status === true){
                                                v = <View key={i} style={{width:70, height:70, borderWidth:1, borderRadius:10, alignItems:'center', justifyContent:'center', marginTop:10, marginRight:10}}>
                                                        <TouchableOpacity
                                                            onPress={() => {
                                                                if(item.type === 'pdf'){
                                                                    navigation.navigate('viewpdfhistory', {path: item.path})
                                                                }else if(item.type === 'png' || item.type === 'jpg' || item.type === 'jpeg'){
                                                                    navigation.navigate('viewimagehistory', {path: item.path})
                                                                }
                                                            }}
                                                        > 
                                                            {icon}
                                                        </TouchableOpacity>
                                                    </View>
                                            }else{
                                                v = <View key={i} style={{alignItems:'center', marginTop:10}}>    
                                                        <Text>{item.message}</Text>
                                                    </View>
                                            }
                                            return v
                                        })
                                    }
                                </ScrollView>
                            </View>
                        </ScrollView>
                    </>
            }else{
                v = <View style={{flex:1, justifyContent:'center', alignItems:'center', margin:10}}>
                        <Text style={{textAlign:'center', fontSize:16}}>{message ? 'Data Tidak Ditemukan' : 'Monitor Pengaduan Anda Disini'}</Text>
                    </View>
            }
            
        }

        return v
    }

    useEffect(() => {
        getData()
    }, [])

    return (
            <View style={{flex:1}}>
                {viewData()}
            </View>
    )
}

export default MonitoringHistoriScreen
