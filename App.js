import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import CekUpdateScreen from './src/CekUpdateScreen.js';
import Routers from './src/Routers.js';

const Stack = createStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
        }}>
        <Stack.Screen
          name="cekupdate"
          component={CekUpdateScreen}
        />
        <Stack.Screen name="Routers" component={Routers} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
